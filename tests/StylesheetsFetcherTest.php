<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Fetcher\StylesheetsFetcher;

final class StylesheetsFetcherTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testStylesheetsFetcherFromDefault(): void
    {
        $url = self::TEST_DOMAIN.'/styles.css';
        $fetcher = StylesheetsFetcher::buildFromUrlString($url);

        $this->assertSame(200, $fetcher->getResponseCode());
        $this->assertSame('text/css', $fetcher->getContentType());
        $this->assertTrue(strlen($fetcher->getContent()) > 0);

        $url = self::TEST_DOMAIN.'/styles_not_exists.css';
        $fetcher = StylesheetsFetcher::buildFromUrlString($url);
        $this->assertSame(404, $fetcher->getResponseCode());
    }
}
