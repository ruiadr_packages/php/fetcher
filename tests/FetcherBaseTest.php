<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Wrapper\Url;
use Ruiadr\Fetcher\Base\FetcherBase;

final class FetcherBaseTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    private function assertFetcher(string $url, string $type, string $contentType): void
    {
        $fetcher = FetcherBase::buildFetcher($type, new Url($url));

        $this->assertSame(200, $fetcher->getResponseCode());
        $this->assertSame($contentType, $fetcher->getContentType());
        $this->assertTrue(strlen($fetcher->getContent()) > 0);
    }

    public function testHtmlFetcherFromDefault(): void
    {
        $url = self::TEST_DOMAIN;
        $this->assertFetcher($url, 'html', 'text/html');

        $url = self::TEST_DOMAIN.'/scripts.js';
        $this->assertFetcher($url, 'javascript', 'application/javascript');

        $url = self::TEST_DOMAIN.'/styles.css';
        $this->assertFetcher($url, 'stylesheets', 'text/css');

        $url = self::TEST_DOMAIN.'/text.txt';
        $this->assertFetcher($url, 'text', 'text/plain');
    }

    public function testGetUrl(): void
    {
        $url = new Url(self::TEST_DOMAIN);
        $fetcher = FetcherBase::buildFetcher('html', $url);
        $this->assertSame($url, $fetcher->getUrl());
    }

    public function testErrors(): void
    {
        $url = self::TEST_DOMAIN.'/styles_not_exists.js';
        $fetcher = FetcherBase::buildFetcher('javascript', new Url($url));
        $this->assertSame(404, $fetcher->getResponseCode());

        $url = self::TEST_DOMAIN.'/styles_not_exists.css';
        $fetcher = FetcherBase::buildFetcher('stylesheets', new Url($url));
        $this->assertSame(404, $fetcher->getResponseCode());

        $url = self::TEST_DOMAIN.'/text_not_exists.txt';
        $fetcher = FetcherBase::buildFetcher('text', new Url($url));
        $this->assertSame(404, $fetcher->getResponseCode());
    }
}
