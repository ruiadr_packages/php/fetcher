<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Fetcher\HtmlFetcher;

final class HtmlFetcherTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testHtmlFetcherFromDefault(): void
    {
        $fetcher = HtmlFetcher::buildFromUrlString(self::TEST_DOMAIN);

        $this->assertSame(200, $fetcher->getResponseCode());
        $this->assertSame('text/html', $fetcher->getContentType());
        $this->assertTrue(strlen($fetcher->getContent()) > 0);
    }
}
