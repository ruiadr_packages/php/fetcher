<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Fetcher\JavascriptFetcher;

final class JavascriptFetcherTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testJavascriptFetcherFromDefault(): void
    {
        $url = self::TEST_DOMAIN.'/scripts.js';
        $fetcher = JavascriptFetcher::buildFromUrlString($url);

        $this->assertSame(200, $fetcher->getResponseCode());
        $this->assertSame('application/javascript', $fetcher->getContentType());
        $this->assertTrue(strlen($fetcher->getContent()) > 0);

        $url = self::TEST_DOMAIN.'/scripts_not_exists.js';
        $fetcher = JavascriptFetcher::buildFromUrlString($url);
        $this->assertSame(404, $fetcher->getResponseCode());
    }
}
