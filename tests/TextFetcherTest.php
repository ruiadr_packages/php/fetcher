<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Fetcher\TextFetcher;

final class TextFetcherTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testTextFetcherFromDefault(): void
    {
        $url = self::TEST_DOMAIN.'/text.txt';
        $fetcher = TextFetcher::buildFromUrlString($url);

        $this->assertSame(200, $fetcher->getResponseCode());
        $this->assertSame('text/plain', $fetcher->getContentType());
        $this->assertTrue(strlen($fetcher->getContent()) > 0);

        $url = self::TEST_DOMAIN.'/text_not_exists.txt';
        $fetcher = TextFetcher::buildFromUrlString($url);
        $this->assertSame(404, $fetcher->getResponseCode());
    }
}
