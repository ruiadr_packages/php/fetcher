<?php

namespace Ruiadr\Fetcher;

use Ruiadr\Fetcher\Base\FetcherBase;

class HtmlFetcher extends FetcherBase
{
    final public function getContentType(): string
    {
        return 'text/html';
    }
}
