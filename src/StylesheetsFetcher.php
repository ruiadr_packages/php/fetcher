<?php

namespace Ruiadr\Fetcher;

use Ruiadr\Fetcher\Base\FetcherBase;

class StylesheetsFetcher extends FetcherBase
{
    final public function getContentType(): string
    {
        return 'text/css';
    }
}
