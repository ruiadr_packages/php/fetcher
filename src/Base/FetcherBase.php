<?php

namespace Ruiadr\Fetcher\Base;

use Ruiadr\Base\Wrapper\CUrl;
use Ruiadr\Base\Wrapper\Interface\CUrlInterface;
use Ruiadr\Base\Wrapper\Interface\UrlInterface;
use Ruiadr\Base\Wrapper\Reflection;
use Ruiadr\Base\Wrapper\Url;
use Ruiadr\Fetcher\Base\Interface\FetcherBaseInterface;

abstract class FetcherBase implements FetcherBaseInterface
{
    private ?CUrlInterface $curl = null;

    /**
     * @param UrlInterface $url Url de la ressource à exploiter
     */
    public function __construct(private readonly UrlInterface $url)
    {
    }

    final public function getUrl(): UrlInterface
    {
        return $this->url;
    }

    final public static function buildFromUrlString(string $urlString): FetcherBaseInterface
    {
        return new static(new Url($urlString));
    }

    final public static function buildFetcher(string $name, UrlInterface $url): ?FetcherBaseInterface
    {
        $fetcherClass = Reflection::buildSuffix($name, 'fetcher');

        return $fetcherClass instanceof \ReflectionClass
            ? $fetcherClass->newInstance($url) : null;
    }

    /**
     * Construit l'objet CUrlInterface.
     *
     * @return CUrlInterface Instance unique de CUrlInterface
     */
    private function getCUrl(): CUrlInterface
    {
        return $this->curl ??= new CUrl($this->url);
    }

    final public function getResponseCode(): ?int
    {
        return $this->getCUrl()->getResponseCode();
    }

    final public function getContent(): string
    {
        $curl = $this->getCUrl()->exec();
        $response = $curl->getResponse();

        return is_string($response) && $curl->isContentType($this->getContentType())
            ? $response : '';
    }
}
