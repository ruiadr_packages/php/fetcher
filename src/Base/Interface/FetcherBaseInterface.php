<?php

namespace Ruiadr\Fetcher\Base\Interface;

use Ruiadr\Base\Wrapper\Interface\UrlInterface;

interface FetcherBaseInterface
{
    /**
     * Objet Url utilisé pour la construction de l'objet.
     *
     * @return UrlInterface Url utilisée par le fetcher
     */
    public function getUrl(): UrlInterface;

    /**
     * Construit un objet FetcherBaseInterface à partir de $urlString
     * passée en paramètre.
     *
     * @param string $urlString Url utilisée pour la construction du fetcher
     *
     * @return FetcherBaseInterface Une instance du fetcher
     */
    public static function buildFromUrlString(string $urlString): FetcherBaseInterface;

    /**
     * Construit un objet FetcherBaseInterface à partir du nom d'un fetcher disponible
     * et d'un objet $url passé en paramètre.
     * Si le fetcher ne peut être construit, la méthode retourne null.
     *
     * @param string       $name Nom du fetcher à construire
     * @param UrlInterface $url  Objet Url utilisé pour la construction du fetcher
     *
     * @return FetcherBaseInterface Une instance du fetcher, ou null si aucun fetcher n'a
     *                              pu être construit
     */
    public static function buildFetcher(string $name, UrlInterface $url): ?FetcherBaseInterface;

    /**
     * Retourne le code HTTP de l'url ciblée.
     *
     * @return ?int Le code HTTP résultant du fetch, null en cas de problème
     */
    public function getResponseCode(): ?int;

    /**
     * Retourne le contenu de l'url ciblée à condition qu'il soit du type
     * retourné par la méthode "getContentType()". En cas de problème, la méthode
     * retourne une chaîne vide signifiant l'absence de résultat cohérent.
     *
     * @return string La réponse, peut être vide en cas de problème
     */
    public function getContent(): string;

    /**
     * Méthode à surcharger pour spécifier le "Content-Type" de retour
     * de l'url qui est ciblée lors de la construction de l'objet.
     *
     * @return string Le "Content-Type" attendu de la cible fetchée
     */
    public function getContentType(): string;
}
