<?php

namespace Ruiadr\Fetcher;

use Ruiadr\Fetcher\Base\FetcherBase;

class TextFetcher extends FetcherBase
{
    final public function getContentType(): string
    {
        return 'text/plain';
    }
}
