<?php

namespace Ruiadr\Fetcher;

use Ruiadr\Fetcher\Base\FetcherBase;

class JavascriptFetcher extends FetcherBase
{
    final public function getContentType(): string
    {
        return 'application/javascript';
    }
}
